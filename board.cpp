
class Board;
class Cell
{
private:
    int x,y;
    const Board *board;
public:
    Cell(int _x=0,int _y=0):x(_x),y(_y),board(0) {}
    void setData(int _x=0,int _y=0, const Board *board=0) {
        x=_x;
        y=_y;
        this->board=board;
    }
};
class Board
{
private:
    Cell cells[8][8];
public:
    Board() {
        for(int x=0;x<8;x++)
            for(int y=0;y<8;y++)
                cells[y][x].setData(x,y,this);
    }
};

int main()
{
    Board board;
    return 0;
}